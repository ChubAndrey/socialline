//
//  TwitterNewsViewController.m
//  SocialLine
//
//  Created by Andrey on 5/19/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import "TwitterNewsViewController.h"

@interface TwitterNewsViewController ()

@end

@implementation TwitterNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    TWTRAPIClient *client = [[TWTRAPIClient alloc] init];
//    self.dataSource = [[TWTRUserTimelineDataSource alloc] initWithScreenName:@"Chub_Andrew" APIClient:client];
    self.dataSource = [[TWTRUserTimelineDataSource alloc] initWithScreenName:@"Chub_Andrew" userID:@"4888067902" APIClient:client maxTweetsPerRequest:5 includeReplies:NO includeRetweets:NO];
    
    NSLog(@"");
    
}

@end
