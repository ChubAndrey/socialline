//
//  VKNews.h
//  SocialLine
//
//  Created by Andrey on 5/25/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VKontakteNews : NSObject

@property (copy, nonatomic) NSArray *urlsPhoto604;
@property (copy, nonatomic) NSArray *urlsPhoto800;
@property (copy, nonatomic) NSString *text;

+ (NSMutableArray *)getNewsesFromDict:(NSDictionary *)dict;

@end
