//
//  FacebookManager.h
//  SocialLine
//
//  Created by Andrey on 5/15/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface FacebookManager : NSObject

+ (instancetype)sharedInstance;

- (void)loginFromViewController:(UIViewController *)vc success:(void(^)(FBSDKLoginManagerLoginResult *result))success failure:(void(^)(NSError *error))failure;

@end
