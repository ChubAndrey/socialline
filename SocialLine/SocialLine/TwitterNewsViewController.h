//
//  TwitterNewsViewController.h
//  SocialLine
//
//  Created by Andrey on 5/19/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TwitterKit/TwitterKit.h>

@interface TwitterNewsViewController : TWTRTimelineViewController

@end
