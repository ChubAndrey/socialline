//
//  VkontakteNewsViewController.m
//  SocialLine
//
//  Created by Andrey on 5/26/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import "VkontakteNewsViewController.h"
#import "FacebookManager.h"
#import "TwitterManager.h"
#import "Macros.h"
#import "VKontakteManager.h"
#import "TwitterNewsViewController.h"
#import "VKApiUsers.h"
#import "VKApiWall.h"
#import "VKApiGroups.h"
#import "VKontakteNews.h"
#import "VKontakteGroup.h"
#import "VKontakteUserProfile.h"
#import "VKontakteTableViewCell.h"
#import "VkontakteResponse.h"

@interface VkontakteNewsViewController ()

@property (strong, nonatomic) NSMutableArray *newsArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (assign, nonatomic) BOOL isPageRefreshing;
@property (strong, nonatomic) NSNumber *countVKNewsArray;


@end

@implementation VkontakteNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.newsArray = [[NSMutableArray alloc] init];
    self.countVKNewsArray = @0;
//    [self loadVKDataWithCount:0];
}

- (void)loadVKDataWithCount:(NSNumber *)count {
    
    [[VKontakteManager sharedInstance] getUserNewsWithCount:count succes:^(VKResponse *response) {
        
        self.countVKNewsArray = @(self.newsArray.count);
        
        NSError *e = [[NSError alloc] init];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[response.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error: &e];
        
        VkontakteResponse *myResponse = [[VkontakteResponse alloc] init];
        [VKontakteManager sharedInstance].vkontackteResponse = myResponse;
        
        [myResponse parseResponseDictionary:dict];
        
//        NSArray *groupsArray = [VKontakteGroup getGroupsFromArrayDicts:myResponse.groups];
//        NSArray *profilesArray = [VKontakteUserProfile getGroupsFromArrayDicts:myResponse.profiles];
        
        NSArray *tempArray = [VKontakteNews getNewsesFromDict:dict];
        [self.newsArray addObjectsFromArray:tempArray];
        
        [self.tableView reloadData];
        
        if ([self.countVKNewsArray integerValue] != self.newsArray.count) {
            self.isPageRefreshing = NO;
        }
        else {
            
            self.isPageRefreshing = YES;
        }
        
        NSLog(@"");
        
    } failure:^(NSError *error) {
        
        NSLog(@"Error");
        
    }];
    
}

- (IBAction)reloadBtnPressed:(id)sender {
    
//    [self loadVKData];
}


#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.newsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VKontakteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VKontakteTableViewCell"];
    
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"VKontakteTableViewCell" owner:self options:nil][0];
    }
    
    [cell setContentData:[self.newsArray objectAtIndex:indexPath.row]];
    
    return cell;
}


#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if(self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height))
    {
        if(self.isPageRefreshing == NO){
            self.isPageRefreshing = YES;
            
            NSArray *test = [[VKontakteManager sharedInstance].vkontackteResponse.nextFrom componentsSeparatedByString:@"/"];
            [self loadVKDataWithCount:[VKontakteManager sharedInstance].vkontackteResponse.nextFrom == nil ? @0 : [test firstObject]];
        }
    }
}

@end
