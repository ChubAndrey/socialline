//
//  VKNews.m
//  SocialLine
//
//  Created by Andrey on 5/25/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import "VKontakteNews.h"

@implementation VKontakteNews

+ (NSMutableArray *)getNewsesFromDict:(NSDictionary *)dict {

    NSMutableArray *mArray = [[NSMutableArray alloc] init];
    
    NSArray *array = [dict valueForKeyPath:@"response.items"];
    
    for (int i = 0; i < array.count; i++) {
        
        NSDictionary *d = [array objectAtIndex:i];
        
        VKontakteNews *vkItem = [[VKontakteNews alloc] init];
        
        if ([d valueForKey:@"attachments"] != nil) {
            
            NSDictionary *attach = [d valueForKey:@"attachments"];
            
            NSArray *photo = [attach valueForKey:@"photo"];
            
            if (![photo[0] isKindOfClass:[NSNull class]]) {
                
                vkItem.urlsPhoto604 = [[attach valueForKeyPath:@"photo"] valueForKey:@"photo_604"];
                vkItem.urlsPhoto800 = [[attach valueForKeyPath:@"photo"] valueForKey:@"photo_800"];
                vkItem.text = [d valueForKey:@"text"];
                
                NSLog(@"%@", vkItem.urlsPhoto604);
                
                [mArray addObject:vkItem];
                
                NSLog(@"");
                
            } else {
                
                NSLog(@"");
            }
            
        } else {
            
             NSLog(@"");
            
        }
        
    }
    
    return mArray;
}

@end
