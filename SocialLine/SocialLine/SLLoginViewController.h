//
//  SLLoginViewController.h
//  SocialLine
//
//  Created by Andrey on 5/15/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VKSdk.h"

@interface SLLoginViewController : UIViewController <VKSdkDelegate, VKSdkUIDelegate>

@property(strong, nonatomic) VKRequest *callingRequest;

@end
