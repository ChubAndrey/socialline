//
//  TwitterManager.h
//  SocialLine
//
//  Created by Andrey on 5/15/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TwitterKit/TwitterKit.h>


@interface TwitterManager : NSObject

+ (instancetype)sharedInstance;

- (void)loginWithSucces:(void(^)(TWTRSession *session))success failure:(void(^)(NSError *error))failure;


@end
