//
//  VkontakteResponse.m
//  SocialLine
//
//  Created by Andrey on 6/8/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import "VkontakteResponse.h"

@implementation VkontakteResponse

- (void)parseResponseDictionary:(NSDictionary *)dictionary {
    
    _groups = [dictionary valueForKeyPath:@"response.groups"];
    _items  = [dictionary valueForKeyPath:@"response.items"];
    _profiles = [dictionary valueForKeyPath:@"response.profiles"];
    _nextFrom = [dictionary valueForKeyPath:@"response.next_from"];
}

@end
