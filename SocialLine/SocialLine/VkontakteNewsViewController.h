//
//  VkontakteNewsViewController.h
//  SocialLine
//
//  Created by Andrey on 5/26/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VkontakteNewsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>

@end
