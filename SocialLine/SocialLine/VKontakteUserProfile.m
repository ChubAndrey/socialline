//
//  VkontakteUserProfile.m
//  SocialLine
//
//  Created by Andrey on 5/26/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import "VKontakteUserProfile.h"

@implementation VKontakteUserProfile

+ (NSMutableArray *)getGroupsFromArrayDicts:(NSArray *)dicts {
    
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < dicts.count; i++) {
        
        NSDictionary *profile = [dicts objectAtIndex:i];
        
        VKontakteUserProfile *vProfileModel = [[VKontakteUserProfile alloc] init];
        
        vProfileModel.firstName     = [profile valueForKey:@"first_name"];
        vProfileModel.profileID     = [profile valueForKey:@"id"];
        vProfileModel.lastName      = [profile valueForKey:@"last_name"];
        vProfileModel.isOnlime      = [profile valueForKey:@"online"];
        vProfileModel.photo50       = [profile valueForKey:@"photo_50"];
        vProfileModel.photo100      = [profile valueForKey:@"photo_100"];
        vProfileModel.screenName    = [profile valueForKey:@"screen_name"];
        vProfileModel.sex           = [profile valueForKey:@"sex"];
        
        [resultArray addObject:vProfileModel];
    }
    
    return resultArray;
}

@end
