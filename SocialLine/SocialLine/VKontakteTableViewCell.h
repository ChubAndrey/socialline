//
//  VKontakteTableViewCell.h
//  SocialLine
//
//  Created by Andrey on 6/1/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VKontakteNews.h"

@interface VKontakteTableViewCell : UITableViewCell

- (void)setContentData:(VKontakteNews *)news;

@end
