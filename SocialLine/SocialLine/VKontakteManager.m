//
//  VKontakteManager.m
//  SocialLine
//
//  Created by Andrey on 5/19/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import "VKontakteManager.h"
#import "VkontakteNewsViewController.h"
#import "Macros.h"

static NSArray *SCOPE = nil;

@interface VKontakteManager ()

@end

@implementation VKontakteManager

+ (instancetype)sharedInstance {
    
    static VKontakteManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[VKontakteManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

- (void)loginWithVKDelegate:(id<VKSdkDelegate>)delegate uiDelegate:(id<VKSdkUIDelegate>)uiDelegate {
    
    SCOPE = @[VK_PER_FRIENDS, VK_PER_WALL, VK_PER_AUDIO, VK_PER_PHOTOS, VK_PER_NOHTTPS, VK_PER_EMAIL, VK_PER_MESSAGES];
    [[VKSdk initializeWithAppId:@"5470754"] registerDelegate:delegate];
    [[VKSdk instance] setUiDelegate:uiDelegate];
    
    [VKSdk wakeUpSession:SCOPE completeBlock:^(VKAuthorizationState state, NSError *error) {
        
        if (state == VKAuthorizationInitialized) {
            [VKSdk authorize:SCOPE];
        }
        else if (state == VKAuthorizationAuthorized) {
            
            VkontakteNewsViewController *vc = InstantiateViewControllerWithIdentifier(@"VkontakteNewsViewController");
            [self.actionVC.navigationController pushViewController:vc animated:YES];
            
            NSLog(@"");
            
        } else if (error) {
            
            NSLog(@"Error %@", error);
        }
    }];
}

- (void)getUserWithSucces:(void (^)(VKResponse *response))succes failure:(void(^)(NSError *error))failure {
    
    VKRequest *request = [[VKApi users] get];
    [request executeWithResultBlock:^(VKResponse *response) {
        
        if (succes) succes(response);
        
        NSLog(@"Result: %@", response);
        
    } errorBlock:^(NSError *error) {
        
        if (error) failure(error);
        
        NSLog(@"Error: %@", error);
    }];
    
}

- (void)getUserNewsWithCount:(NSNumber *)count succes:(void (^)(VKResponse *response))succes failure:(void(^)(NSError *error))failure {
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"start_from"] = count;
    
    [[VKRequest requestWithMethod:@"newsfeed.get" parameters:params] executeWithResultBlock:^(VKResponse *response){
        
        if (succes) succes(response);
        
        NSLog(@"Succes");
        
    } errorBlock:^(NSError *error) {
        
        if (error) failure(error);
        
        NSLog(@"Error");
    }];
}

@end
