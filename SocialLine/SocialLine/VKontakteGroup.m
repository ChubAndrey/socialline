//
//  VKontakteGroup.m
//  SocialLine
//
//  Created by Andrey on 5/26/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import "VKontakteGroup.h"

@implementation VKontakteGroup

+ (NSMutableArray *)getGroupsFromArrayDicts:(NSArray *)dicts {
    
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < dicts.count; i++) {
        
        NSDictionary *group = [dicts objectAtIndex:i];
        
        VKontakteGroup *vkGroupModel = [[VKontakteGroup alloc] init];
        
        vkGroupModel.groupID    = [group valueForKey:@"id"];
        vkGroupModel.isClosed   = [group valueForKey:@"is_closed"];
        vkGroupModel.name       = [group valueForKey:@"name"];
        vkGroupModel.photo50    = [group valueForKey:@"photo_50"];
        vkGroupModel.photo100   = [group valueForKey:@"photo_100"];
        vkGroupModel.photo200   = [group valueForKey:@"photo_200"];
        vkGroupModel.screenName = [group valueForKey:@"screen_name"];
        
        [resultArray addObject:vkGroupModel];
    }
    
    return resultArray;
}

@end
