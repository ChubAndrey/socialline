//
//  VkontakteResponse.h
//  SocialLine
//
//  Created by Andrey on 6/8/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VkontakteResponse : NSObject

@property (copy, nonatomic) NSArray     *groups;
@property (copy, nonatomic) NSArray     *items;
@property (copy, nonatomic) NSArray     *profiles;
@property (copy, nonatomic) NSString    *nextFrom;

- (void)parseResponseDictionary:(NSDictionary *)dictionary;

@end
