//
//  VkontakteUserProfile.h
//  SocialLine
//
//  Created by Andrey on 5/26/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VKontakteUserProfile : NSObject

@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSNumber *profileID;
@property (copy, nonatomic) NSString *lastName;
@property (copy, nonatomic) NSNumber *isOnlime;
@property (copy, nonatomic) NSString *photo100;
@property (copy, nonatomic) NSString *photo50;
@property (copy, nonatomic) NSString *screenName;
@property (copy, nonatomic) NSNumber *sex;

+ (NSMutableArray *)getGroupsFromArrayDicts:(NSArray *)dicts;

@end
