//
//  VKontakteManager.h
//  SocialLine
//
//  Created by Andrey on 5/19/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "VKSdk.h"
#import "VKontakteNews.h"
#import "VkontakteResponse.h"

@interface VKontakteManager : NSObject

@property (strong, nonatomic) UIViewController *actionVC;
@property (strong, nonatomic) VkontakteResponse *vkontackteResponse;

+ (instancetype)sharedInstance;

- (void)loginWithVKDelegate:(id<VKSdkDelegate>)delegate uiDelegate:(id<VKSdkUIDelegate>)uiDelegate;
- (void)getUserWithSucces:(void (^)(VKResponse *response))succes failure:(void(^)(NSError *error))failure;
- (void)getUserNewsWithCount:(NSNumber *)count succes:(void (^)(VKResponse *response))succes failure:(void(^)(NSError *error))failure;
@end
