//
//  VKontakteTableViewCell.m
//  SocialLine
//
//  Created by Andrey on 6/1/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import "VKontakteTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface VKontakteTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *contentImageVIew;

@end

@implementation VKontakteTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setContentData:(VKontakteNews *)news {
    
    self.titleTextLabel.text = news.text;
    [self.contentImageVIew sd_setImageWithURL:[NSURL URLWithString:news.urlsPhoto604.count !=0 ? news.urlsPhoto604[0] : news.urlsPhoto800[0]]  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageUrl){
        
    }];
    
    NSLog(@"");
}

@end
