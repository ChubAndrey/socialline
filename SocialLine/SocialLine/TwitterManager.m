//
//  TwitterManager.m
//  SocialLine
//
//  Created by Andrey on 5/15/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import "TwitterManager.h"

@interface TwitterManager ()

@end

@implementation TwitterManager

+ (instancetype)sharedInstance {
    
    static TwitterManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[TwitterManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

- (void)loginWithSucces:(void(^)(TWTRSession *session))success failure:(void(^)(NSError *error))failure {
    
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            
            NSLog(@"signed in as %@", [session userName]);
            success(session);
            
            TWTRAPIClient *client = [[TWTRAPIClient alloc] init];
            NSMutableArray *mArray = [[NSMutableArray alloc] init];
            mArray = [[TWTRUserTimelineDataSource alloc] initWithScreenName:@"Test" APIClient:client];
            NSLog(@"");
            
        } else {
            
            NSLog(@"error: %@", [error localizedDescription]);
            failure(error);
            
        }
    }];
}

@end
