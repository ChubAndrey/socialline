//
//  SLLoginViewController.m
//  SocialLine
//
//  Created by Andrey on 5/15/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import "SLLoginViewController.h"
#import "FacebookManager.h"
#import "TwitterManager.h"
#import "Macros.h"
#import "VKontakteManager.h"
#import "TwitterNewsViewController.h"
#import "VKApiUsers.h"
#import "VKApiWall.h"
#import "VKApiGroups.h"
#import "VKontakteNews.h"
#import "VkontakteNewsViewController.h"


@interface SLLoginViewController ()

@end

@implementation SLLoginViewController


#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}


#pragma mark - Aktion

- (IBAction)vkBtnPressed:(id)sender {
    
    NSLog(@"vkBtnPressed");
    
    [VKontakteManager sharedInstance].actionVC = self;
    
    [[VKontakteManager sharedInstance] loginWithVKDelegate:self uiDelegate:self];

}

- (IBAction)twitterBtnPressed:(id)sender {
    
    NSLog(@"twitterBtnPressed");
    
    [[TwitterManager sharedInstance] loginWithSucces:^(TWTRSession *session) {
        
        NSLog(@"%@", session);
        TwitterNewsViewController *vc = InstantiateViewControllerWithIdentifier(@"TwitterNewsViewController");
        [self.navigationController pushViewController:vc animated:YES];
        
    } failure:^(NSError *error) {
        
        NSLog(@"%@", error);
    }];
}

- (IBAction)instagramBtnPressed:(id)sender {
    
    NSLog(@"instagramBtnPressed");
}

- (IBAction)fbBtnPressed:(id)sender {
    
    NSLog(@"fbBtnPressed");
    
    [[FacebookManager sharedInstance] loginFromViewController:self success:^(FBSDKLoginManagerLoginResult *result) {
        
        NSLog(@"%@", result);
        
    }failure:^(NSError *error) {
        
        NSLog(@"%@", error);
    }];
}

- (IBAction)lookAllBtnPressed:(id)sender {
    
}


#pragma mark - VKDelegate

- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken {
    
    NSLog(@"");
}

- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError {
    
    NSLog(@"");
}

- (void)vkSdkUserAuthorizationFailed {
    
    NSLog(@"");
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    
    NSLog(@"");
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {
    
    NSLog(@"");
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
    
    VkontakteNewsViewController *vc = InstantiateViewControllerWithIdentifier(@"VkontakteNewsViewController");
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    
    [self.navigationController.topViewController presentViewController:controller animated:YES completion:nil];
}

@end
