//
//  FacebookManager.m
//  SocialLine
//
//  Created by Andrey on 5/15/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import "FacebookManager.h"

@interface FacebookManager ()

@end

@implementation FacebookManager

+ (instancetype)sharedInstance {
    
    static FacebookManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[FacebookManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

- (void)loginFromViewController:(UIViewController *)vc success:(void(^)(FBSDKLoginManagerLoginResult *result))success failure:(void(^)(NSError *error))failure {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    [login logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"] fromViewController:vc handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error) {
            
            NSLog(@"Process error");
            failure(error);
            
        } else if (result.isCancelled) {
            
            NSLog(@"Cancelled");
            
        } else {
            
            NSLog(@"Logged in");
            success(result);
        }
        
    }];
}

@end
