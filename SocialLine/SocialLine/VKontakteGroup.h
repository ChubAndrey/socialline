//
//  VKontakteGroup.h
//  SocialLine
//
//  Created by Andrey on 5/26/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VKontakteGroup : NSObject

@property (copy, nonatomic) NSNumber *groupID;
@property (copy, nonatomic) NSNumber *isClosed;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *screenName;
@property (copy, nonatomic) NSString *photo50;
@property (copy, nonatomic) NSString *photo100;
@property (copy, nonatomic) NSString *photo200;

+ (NSMutableArray *)getGroupsFromArrayDicts:(NSArray *)dicts;

@end
